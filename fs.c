#include "disk.h"
#include "fs.h"

#include <stdio.h>
#include <string.h>
#include <math.h>

#define min(a,b) (((a) < (b)) ? (a) : (b))

#ifdef DEBUG
#define DEBUG_PRINT(fmt, args...)    fprintf(stderr, fmt, ## args)
#else
#define DEBUG_PRINT(fmt, args...)    /* Don't do anything in release builds */
#endif

// Debug file system -----------------------------------------------------------
int * bitmap = NULL;
int bitmap_size;
Block testBlock;

void fs_debug(Disk *disk)
{
    if (disk == 0)
        return;

    Block block;
    Block block2;

    // Read Superblock
    disk_read(disk, 0, block.Data);

    uint32_t magic_num = block.Super.MagicNumber;
    uint32_t num_blocks = block.Super.Blocks;
    uint32_t num_inodeBlocks = block.Super.InodeBlocks;
    uint32_t num_inodes = block.Super.Inodes;

    if (magic_num != MAGIC_NUMBER)
    {
        printf("Magic number is valid: %c\n", magic_num);
        return;
    }

    printf("SuperBlock:\n");
    printf("    magic number is valid\n");
    printf("    %u blocks\n", num_blocks);
    printf("    %u inode blocks\n", num_inodeBlocks);
    printf("    %u inodes\n", num_inodes);

    uint32_t expected_num_inodeBlocks = round((float)num_blocks / 10);

    if (expected_num_inodeBlocks != num_inodeBlocks)
    {
        printf("SuperBlock declairs %u InodeBlocks but expect %u InodeBlocks!\n", num_inodeBlocks, expected_num_inodeBlocks);
    }

    uint32_t expect_num_inodes = num_inodeBlocks * INODES_PER_BLOCK;
    if (expect_num_inodes != num_inodes)
    {
        printf("SuperBlock declairs %u Inodes but expect %u Inodes!\n", num_inodes, expect_num_inodes);
    }
    
    unsigned int inode_block_count = block.Super.InodeBlocks;

    for (int i = 0; i < inode_block_count; i++) {
        disk_read(disk, i+1, block.Data);

        for (int j = 1; j < INODES_PER_BLOCK; j++) {
            
            if (!block.Inodes[j].Valid) {
                continue;
            }

                printf("Inode %d:\n", j);
                printf("    size: %d bytes\n", block.Inodes[j].Size);
                printf("    direct blocks:");

            for (unsigned int k = 0; k < POINTERS_PER_INODE; k++) {
                if (block.Inodes[j].Direct[k] != 0) {
                   printf(" %d", block.Inodes[j].Direct[k]);
                }
            } 
         

            uint32_t indir;
            indir = block.Inodes[j].Indirect;
            

            if (indir !=0 ) {
                printf("\n");
                printf("    indirect block: %d\n", indir);
                disk_read(disk, indir, block2.Data);
                printf("    indirect data blocks:");

                for (int l =0; l < POINTERS_PER_BLOCK; l++) {
                    if (block2.Pointers[l] != 0) {
                        printf(" %d", block2.Pointers[l]);
                    }
                }
            }


            printf("\n");
        }
    }

    
        
    }

// Format file system ----------------------------------------------------------

bool fs_format(Disk *disk)
{
    if (disk_mounted(disk)) {
        return false;
    }
    
    // Write superblock
    Block block;
    memset(block.Data, 0, BLOCK_SIZE);
    memset(block.Data,0, BLOCK_SIZE);
    block.Super.MagicNumber = MAGIC_NUMBER;
    block.Super.Blocks = disk_size(disk);
    block.Super.InodeBlocks = (size_t)((float)disk_size(disk)*0.1+0.5);
    block.Super.Inodes = INODES_PER_BLOCK*block.Super.InodeBlocks;

    disk_write(disk, 0, block.Data);

    // Clear all other blocks
    char clear[BUFSIZ] = {0};
    for (size_t i=1; i<block.Super.Blocks; i++) {
        disk_write(disk, i, clear);
    }

    return true;
}

// FileSystem constructor 
FileSystem *new_fs()
{
    FileSystem *fs = malloc(sizeof(FileSystem));
    return fs;
}

// FileSystem destructor 
void free_fs(FileSystem *fs)
{
    // FIXME: free resources and allocated memory in FileSystem
    free(fs);
}

// Mount file system -----------------------------------------------------------

bool fs_mount(FileSystem *fs, Disk *disk)
{
    // Read superblock
    Block block;

    disk_read(disk, 0, block.Data);

    if (disk_mounted(disk)) {
        return false;
    }

    if (block.Super.Inodes != block.Super.InodeBlocks * INODES_PER_BLOCK) {
        return false;
    }

    // check magic number
    if (block.Super.MagicNumber != MAGIC_NUMBER) {
        return false;
    }

    // make sure there are blocks
    if (block.Super.Blocks < 0) {
        return false;
    }

    // check inode proportion
    if (block.Super.InodeBlocks != ceil(.1 * block.Super.Blocks)) {
        return false;
    }

    // Set device and mount
    disk_mount(disk);
    fs->disk = disk;
    bitmap = calloc(block.Super.Blocks, sizeof(int));
    bitmap_size = block.Super.Blocks;


    // Copy metadata
    Block blockInode;
    Inode inode;

    for (int i = 0; i < block.Super.InodeBlocks; i++)
    {
        disk_read(disk, i, blockInode.Data);

        inode = blockInode.Inodes[i];

        if(inode.Valid) {
            bitmap[i] = 1;
                for (int d_blocks = 0; d_blocks * 4096 < inode.Size && d_blocks < 5; d_blocks++) {
                    bitmap[inode.Direct[d_blocks]] = 1;
                }
                
                if(inode.Size > 5 * 4096){
                    
                    bitmap[inode.Indirect] = 1;
                    
                    Block temp_block;
                    disk_read(disk, inode.Indirect, temp_block.Data);
                    
                    for( int indirect_block = 0; indirect_block < (double)inode.Size/4096 - 5; indirect_block++){
                        bitmap[temp_block.Pointers[indirect_block]] = 1;
                    }
                }
            }
        }

    return true;
}

// Create inode ----------------------------------------------------------------

ssize_t fs_create(FileSystem *fs)
{

// Locate free inode in inode table

    Block block;
    int ind = -1;
    for (uint32_t inode_block = 0; inode_block < block.Super.InodeBlocks; inode_block++) {
        Block b;
        disk_read(fs->disk, 1+inode_block,b.Data);
        // reads each inode
        for (uint32_t inode = 0; inode < INODES_PER_BLOCK; inode++) {
            // if it's not valid, it's free to be written
            if (!b.Inodes[inode].Valid) {
                ind = inode + INODES_PER_BLOCK*inode_block;
                break;
            }
        }
        if (ind != -1) {
            break;
        }
    }

    // Record inode if found
    if (ind == -1) {
        return -1;
    }
    Inode i;
    i.Valid = true;
    i.Size = 0;
    for (unsigned int j = 0; j < POINTERS_PER_INODE; j++) {
        i.Direct[j] = 0;
    }
    i.Indirect = 0;
    store_inode(fs, ind, &i);

    return ind;

 }




// // Optional: the following two helper functions may be useful. 

bool find_inode(FileSystem *fs, size_t inumber, Inode *inode)
{
    size_t block_number = 1 + (inumber / INODES_PER_BLOCK);
    size_t inode_offset = inumber % INODES_PER_BLOCK;

    if (inumber >= testBlock.Super.Inodes) {
        return false;
    }

    Block block;
    disk_read(fs->disk, block_number, block.Data);

    *inode = block.Inodes[inode_offset];

    return true;
}

bool store_inode(FileSystem *fs, size_t inumber, Inode *inode)
{

    size_t block_number = 1 + inumber / INODES_PER_BLOCK;
    size_t inode_offset = inumber % INODES_PER_BLOCK;

    if (inumber >= testBlock.Super.Inodes) {
        return false;
    }

    Block block;
    disk_read(fs->disk, block_number, block.Data);
    block.Inodes[inode_offset] = *inode;
    disk_write(fs->disk, block_number, block.Data);

    return true;
}

// Remove inode ----------------------------------------------------------------

bool fs_remove(FileSystem *fs, size_t inumber)
{
    Inode node;

    // Load inode information
    if (!find_inode(fs, inumber, &node)) {
        return false;
    }
    if (node.Valid == 0) {
        return false;
    }

    // Free direct blocks
    for (unsigned int i = 0; i < POINTERS_PER_INODE; i++) {
        if (node.Direct[i] != 0) {
            bitmap[node.Direct[i]] = 1;
            node.Direct[i] = 0;
        }
    }

    // Free indirect blocks
    if (node.Indirect != 0) {
        bitmap[node.Indirect] = 1;
        Block b;
        disk_read(fs->disk, node.Indirect,b.Data);
        // Free blocks pointed to indirectly
        for (unsigned int i = 0; i < POINTERS_PER_BLOCK; i++) {
            if (b.Pointers[i] != 0) {
                bitmap[b.Pointers[i]] = 1;
            }
        }
    }
    // Clear inode in inode table
    node.Indirect = 0;
    node.Valid = 0;
    node.Size = 0;
    if (!store_inode(fs, inumber, &node)) {
        return false;
    };

    return true;

}

// Inode stat ------------------------------------------------------------------

ssize_t fs_stat(FileSystem *fs, size_t inumber)
{
    Inode i;
    if (!find_inode(fs, inumber, &i) || !i.Valid) {
        return -1;
    }

    return i.Size;
}

// Read from inode -------------------------------------------------------------

ssize_t fs_read(FileSystem *fs, size_t inumber, char *data, size_t length, size_t offset)
{
    // Load inode information

    // Adjust length

    // Read block and copy to data
    
    return 0;
}

// Optional: the following helper function may be useful. 

// ssize_t fs_allocate_block(FileSystem *fs)
// {
//     return -1;
// }

// Write to inode --------------------------------------------------------------

ssize_t fs_write(FileSystem *fs, size_t inumber, char *data, size_t length, size_t offset)
{
    // Load inode
    
    // Write block and copy to data

    return 0;
}
